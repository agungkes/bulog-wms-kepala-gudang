interface SuratJalanData {
  id: string;
  no_surat_jalan: string;
  no_po: string;
  gudang: string;
  product: string;
  quantity: string;
  created_at: string;
}

interface IncomingData {
  priority: string;
  state: string;
  id: number;
  invoice_paid: number;
  location_id: [number, string] | boolean;
  state_inv: boolean;
  group_id: boolean;
  scheduled_date: string;
  picking_type_id: [number, string] | boolean;
  ref_fax: string;
  batch_id: boolean;
  date: string;
  partner_id: [number, string] | boolean;
  location_dest_id: [number, string] | boolean;
  origin: string;
  name: string;
  backorder_id: boolean;
  move_lines: number[];
  move_line_ids: number[];
}

interface IncomingDetail {
  branch_id: [number, string] | boolean;
  move_lines?: number[];
  backorder_id: boolean;
  batch_id: boolean;
  date: string;
  group_id: boolean;
  id: number;
  invoice_paid: number;
  location_dest_id: [number, string] | boolean;
  location_id: [number, string] | boolean;
  name: string;
  origin: string;
  partner_id: [number, string] | boolean;
  picking_type_id: [number, string] | boolean;
  priority: string;
  ref_fax: string;
  scheduled_date: string;
  state: string;
  state_inv: boolean;
}

interface IncomingMoving {
  sh_sec_uom: [number, string] | boolean;
  jt2: number;
  is_quantity_done_editable: boolean;
  sh_is_secondary_unit: boolean;
  show_details_visible: boolean;
  name: string;
  is_locked: boolean;
  quantity_done: number;
  date_jt2: boolean;
  product_id: [number, string] | boolean;
  reserved_availability: number;
  qty_picking: number;
  qc_status: boolean;
  sh_sec_done_qty: number;
  state: string;
  product_uom_qty: number;
  sh_sec_qty: number;
  analytic_account_id: [number, string] | boolean;
  digital_analog: number;
  jtnetto: number;
  show_operations: boolean;
  scrapped: boolean;
  has_move_lines: boolean;
  date_jt1: boolean;
  location_dest_id: [number, string] | boolean;
  jt1: number;
  picking_type_id: [number, string] | boolean;
  is_initial_demand_editable: boolean;
  additional: boolean;
  id: number;
  branch_id: [number, string] | boolean;
  location_id: [number, string] | boolean;
  origin_returned_move_id: boolean;
  show_reserved_availability: boolean;
  product_uom: [number, string] | boolean;
  date_expected: string;
  product_type: string;
  qty_putaway: number;
  sec_uom_analog: [number, string] | boolean;
  picking_code: string;
}

interface PutawayData {
  put_origin: string;
  display_name: string;
  message_follower_ids: number[];
  picking_type_id: [number, string];
  name: string;
  put_dest_location_id: [number, string];
  picking_type_code: string;
  state: string;
  id: number;
  date: string;
  work_number: number;
  note: boolean;
  tools_name: boolean;
  company_id: [number, string];
  move_ids: any[];
  put_picking_id: [number, string];
  partner_id: [number, string];
  message_ids: number[];
  tools_number: number;
  leader_name: boolean;
  put_source_location_id: [number, string];
}

interface PickingData {
  pick_picking_id: [number, string];
  company_id: [number, string];
  display_name: string;
  message_follower_ids: number[];
  picking_type_id: [number, string];
  pick_origin: string;
  name: string;
  picking_type_code: string;
  state: string;
  id: number;
  date: string;
  work_number: number;
  note: boolean;
  tools_name: boolean;
  message_ids: number[];
  move_ids: number[];
  partner_id: [number, string];
  tools_number: number;
  leader_name: boolean;
  pick_source_location_id: [number, string];
  pick_dest_location_id: [number, string];
}

interface PickingProduct {
  is_quantity_done_editable: boolean;
  show_details_visible: boolean;
  has_move_lines: boolean;
  is_locked: boolean;
  quantity_done: number;
  product_id: [number, string];
  reserved_availability: number;
  additional: boolean;
  state: string;
  product_uom_qty: number;
  location_dest_id: [number, string];
  scrapped: boolean;
  picking_type_id: [number, string];
  is_initial_demand_editable: boolean;
  id: number;
  location_id: [number, string];
  show_operations: boolean;
  show_reserved_availability: boolean;
  product_uom: [number, string];
  picking_code: boolean;
  product_type: string;
  date_expected: string;
  name: string;
}

interface GetDailyMonitoring {
  month: string;
  name: string;
  state: string;
  id: number;
  gudang_id: [number, string] | boolean;
  date: string;
  unit_gudang: string;
  kepala_gudang_id: [number, string] | boolean;
  aerasi_line_ids: number[];
  company_id: [number, string];
  location_id: [number, string];
  pengosongan_gudang_ids: number[];
  sanitasi_gudang_ids: number[];
  warehouse_id: [number, string] | boolean;
  kualitas_awal_ids: number[];
}

interface QuestionResponse {
  result: boolean;
  result1: boolean;
  type: string;
  id: number;
  qna_id: [number, string];
}
