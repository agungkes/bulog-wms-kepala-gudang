import 'react-native-gesture-handler';

import { Navigation } from 'react-native-navigation';
import screens from './src/screens';

import { configurePersistable } from 'mobx-persist-store';
import AsyncStorage from '@react-native-community/async-storage';
import { registerScreens } from 'react-native-navigation-register-screens';

import Provider from './src/components/Provider';
import App from './App';
import { storeServer } from 'stores/server.stores';
import { Dimensions } from 'react-native';

import Home from '@modules/Home';
import Authentication from '@modules/Authentication';
import ScanQr from '@modules/ScanQr';
import { SuratJalanDetail, SuratJalanList } from '@modules/SuratJalan';
import { IncomingDetail, IncomingList } from '@modules/Incoming';
import { PutawayDetail, PutawayList } from '@modules/Putaway';
import Notification from '@modules/Notification';
import { PickingDetail, PickingList } from '@modules/Picking';
import Loading from '@modules/Loading';
import {
  PerawatanGudangCreate,
  PerawatanGudangDetail,
  PerawatanGudangList,
} from 'modules/PerawatanGudang';

import { RNNBottomSheet } from 'react-native-navigation-bottom-sheet';
import PerawatanGudangDaily from 'modules/PerawatanGudang/PerawatanGudangDaily';
import PerawatanGudangDailyCreate from 'modules/PerawatanGudang/PerawatanGudangDailyCreate';
import PerawatanGudangDailyDetail from 'modules/PerawatanGudang/PerawatanGudangDailyDetail';

configurePersistable(
  {
    storage: AsyncStorage,
    expireIn: 86400000 * 30,
    removeOnExpiration: true,
    stringify: true,
    debugMode: false,
  },
  { delay: 200, fireImmediately: false },
);

Navigation.events().registerAppLaunchedListener(() => {
  storeServer.hydrateStore().then(() => {
    if (storeServer.isHydrated) {
      RNNBottomSheet.init();
      registerScreens(
        [
          App,
          Loading,
          Authentication,
          Home,
          Notification,
          ScanQr,
          SuratJalanList,
          SuratJalanDetail,
          IncomingList,
          IncomingDetail,
          PutawayList,
          PutawayDetail,
          PerawatanGudangList,
          PerawatanGudangDetail,
          PerawatanGudangCreate,

          PerawatanGudangDaily,
          PerawatanGudangDailyCreate,
          PerawatanGudangDailyDetail,
          PickingList,
          PickingDetail,
        ],
        Provider,
      );

      Navigation.setDefaultOptions({
        layout: {
          orientation: ['portrait'],
        },
        topBar: {
          visible: true,
          drawBehind: false,
          background: {
            color: '#02418b',
          },
          title: {
            color: '#fff',
          },
          backButton: {
            color: '#fff',
          },
        },
        statusBar: {
          drawBehind: false,
          translucent: false,
          backgroundColor: '#02418b',
        },
        navigationBar: {
          backgroundColor: '#02418b',
        },
        bottomTabs: {
          visible: false,
          drawBehind: true,
        },
        animations: {
          push: {
            waitForRender: true,
            content: {
              x: {
                from: Dimensions.get('window').width,
                to: 0,
                duration: 250,
              },
            },
            topBar: {
              waitForRender: true,
              x: {
                from: Dimensions.get('window').width,
                to: 0,
                duration: 250,
              },
            },
          },
          pop: {
            waitForRender: true,
            content: {
              x: {
                from: 0,
                to: Dimensions.get('window').width,
                duration: 250,
              },
            },
            topBar: {
              waitForRender: true,
              x: {
                from: 0,
                to: Dimensions.get('window').width,
                duration: 250,
              },
            },
          },
        },
      });

      Navigation.setRoot({
        root: {
          stack: {
            children: [
              {
                component: {
                  name: screens.INIT,
                },
              },
            ],
          },
        },
      });
    }
  });
});
