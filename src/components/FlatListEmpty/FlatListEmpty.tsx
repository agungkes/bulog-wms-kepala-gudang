import React from 'react';
import { View } from 'react-native';
import { Image, Text } from 'react-native-elements';
import styles from './FlatListEmpty.styles';

const FlatListEmpty = () => {
  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/images/logo.png')}
        style={styles.emptyLogo}
        resizeMode="contain"
      />
      <Text style={styles.emptyText}>Tidak ada apapun disini</Text>
    </View>
  );
};

export default FlatListEmpty;
