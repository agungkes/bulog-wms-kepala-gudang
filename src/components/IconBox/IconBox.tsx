import React from 'react';
import { ImageSourcePropType, Pressable, View } from 'react-native';
import DropShadow from 'react-native-drop-shadow';
import { Image, Text, useTheme, Icon } from 'react-native-elements';
import styles from './IconBox.styles';

interface Props {
  icon?: {
    name: string;
    size?: number;
    color?: string;
  };
  source?: ImageSourcePropType;
  title: string;
  onPress?: () => void;
}

const IconBox: React.FC<Props> = ({ source, icon, title, onPress }) => {
  const { theme } = useTheme();
  const iconNode = source ? (
    <Image source={source} style={styles.icon} />
  ) : icon ? (
    <Icon
      tvParallaxProperties
      type="ionicon"
      name={icon.name}
      size={icon.size}
      color={icon.color}
    />
  ) : null;
  return (
    <DropShadow style={styles.dropShadow}>
      <View style={styles.container}>
        <Pressable
          android_ripple={{
            borderless: false,
            color: theme.colors?.accent,
          }}
          onPress={onPress}
          style={styles.button}>
          {iconNode}
          <Text style={styles.text}>{title}</Text>
        </Pressable>
      </View>
    </DropShadow>
  );
};

export default IconBox;
