import { StyleSheet } from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  itemLeftText: {
    flexBasis: '30%',
    fontSize: widthPercentageToDP(3.5),
  },
  itemRightText: {
    flexShrink: 1,
    textAlign: 'right',
    fontSize: widthPercentageToDP(3.5),
  },
});
export default styles;
