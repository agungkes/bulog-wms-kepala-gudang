import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  InputContainer: {
    marginBottom: 0,
    height: 30,
  },
});
export default styles;
