import React from 'react';
import { ListItem, Input } from 'react-native-elements';
// import { TextInput } from 'react-native-gesture-handler';
import styles from './ItemDetail.styles';
type ItemDetailProps = {
  title?: string;
  value?: string | number | [number, string] | boolean;
  isShow?: boolean;
  editable?: boolean;
  inputEditable?: boolean;
  onChangeText?: (text: string) => void;
};
const ItemDetail: React.FC<ItemDetailProps> = ({
  title,
  value,
  isShow = true,
  editable = false,
  onChangeText,
  inputEditable = true,
}) => (
  <ListItem hasTVPreferredFocus tvParallaxProperties>
    <ListItem.Content>
      {title && <ListItem.Title>{title}</ListItem.Title>}
      {isShow && !editable && (
        <ListItem.Subtitle>
          {Array.isArray(value) ? value[1] : value}
        </ListItem.Subtitle>
      )}
      {isShow && editable && (
        <Input
          autoCompleteType
          defaultValue={`${(Array.isArray(value) ? value[1] : value) || ''}`}
          onChangeText={onChangeText}
          editable={inputEditable && editable}
          placeholder="Destination Location"
          containerStyle={styles.InputContainer}
        />
      )}
    </ListItem.Content>
  </ListItem>
);

export default ItemDetail;
