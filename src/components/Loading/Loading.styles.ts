import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  loadingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: widthPercentageToDP(1),
  },
  loadingLogo: {
    width: widthPercentageToDP(50),
    height: heightPercentageToDP(7),
    marginRight: widthPercentageToDP(2),
  },
});

export default styles;
