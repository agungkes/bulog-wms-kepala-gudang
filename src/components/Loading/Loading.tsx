import React, { useEffect } from 'react';
import { ActivityIndicator, BackHandler, View } from 'react-native';
import { Image, Overlay } from 'react-native-elements';
import styles from './Loading.styles';

type LoadingProps = {
  isVisible: boolean;
};

const handlePressBack = () => false;
const Loading: React.FC<LoadingProps> = ({ isVisible }) => {
  useEffect(() => {
    if (isVisible) {
      BackHandler.addEventListener('hardwareBackPress', handlePressBack);
    }
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handlePressBack);
    };
  }, [isVisible]);
  return (
    <Overlay isVisible={isVisible}>
      <View style={styles.loadingContainer}>
        <Image
          source={require('../../assets/images/logo.png')}
          style={styles.loadingLogo}
        />
        <ActivityIndicator size="large" color="#e97e11" />
      </View>
    </Overlay>
  );
};

export default Loading;
