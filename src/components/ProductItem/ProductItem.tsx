import React from 'react';
import { TextInputProps } from 'react-native';
import { ListItem } from 'react-native-elements';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

type ProductItemProps = {
  label: string;
  value?: string | number | [number, string] | boolean;
  onChangeText?: (text: string) => void;
  editable?: boolean;
  keyboardType?: TextInputProps['keyboardType'];
  multiline?: boolean;
};
const ProductItem: React.FC<ProductItemProps> = ({
  label,
  value,
  onChangeText,
  editable = false,
  keyboardType,
  multiline = false,
}) => {
  return (
    <ListItem
      hasTVPreferredFocus
      tvParallaxProperties
      containerStyle={{
        paddingHorizontal: widthPercentageToDP(3),
        paddingVertical: heightPercentageToDP(0),
      }}>
      <ListItem.Content>
        <ListItem.Title>{label}</ListItem.Title>
      </ListItem.Content>

      <ListItem.Input
        autoCompleteType
        defaultValue={`${Array.isArray(value) ? value[1] : value}`}
        onChangeText={onChangeText}
        editable={editable}
        keyboardType={keyboardType}
        multiline={multiline}
      />
    </ListItem>
  );
};

export default ProductItem;
