const state: Record<string, string> = {
  draft: 'Draft',
  approved: 'Approved',
  approve: 'Waiting Supervisor Approval',
  done: 'Done',
  cancel: 'Cancel',
  assigned: 'Ready',
};

export default state;
