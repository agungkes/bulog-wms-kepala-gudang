import React, { Fragment } from 'react';
import { Image, KeyboardAvoidingView } from 'react-native';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';

import screens from '../../screens';
import { Text, useTheme } from 'react-native-elements';

import useToggle from '@hooks/useToggle';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

import ChangeServerModal from './components/ChangeServerModal';
import LoginForm from './components/LoginForm';

import styles from './Authentication.styles';
import { observer } from 'mobx-react-lite';
import { useServerStore } from '@stores/server.stores';
import Analytics from 'appcenter-analytics';
import Layout from '@components/Layout';
type AuthenticationProps = {};

export type DetailServer = {
  domain: string;
  database: string;
  protocol: string;
};
const Authentication: ScreenFC<AuthenticationProps> = () => {
  const { theme } = useTheme();
  const server = useServerStore();

  const [showChangeServer, setChangeServer] = useToggle();

  const handleSaveServer = async (detailServer: DetailServer) => {
    try {
      server.setServer = {
        domain: detailServer.domain,
        database: detailServer.database,
        protocol: detailServer.protocol,
      };
      Analytics.trackEvent('Set Server', detailServer);
      setChangeServer();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Fragment>
      <KeyboardAvoidingView>
        <Layout>
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={styles.container}
            keyboardShouldPersistTaps="always">
            <TouchableOpacity onPress={setChangeServer}>
              <Text
                style={[
                  styles.text,
                  styles.textRight,
                  { color: theme?.colors?.primary },
                ]}>
                Ubah Server
              </Text>
            </TouchableOpacity>

            <Image
              source={require('../../assets/images/logo.png')}
              resizeMode="contain"
              style={styles.logo}
            />

            <Text
              style={[
                styles.text,
                styles.textDescription,
                { color: theme?.colors?.primary },
              ]}>
              Warehouse Management System
            </Text>

            <Text
              style={[
                styles.text,
                styles.textDescription,
                { color: theme?.colors?.accent },
              ]}>
              Kepala Gudang
            </Text>

            <LoginForm />
          </ScrollView>
        </Layout>
      </KeyboardAvoidingView>

      <ChangeServerModal
        onSubmit={handleSaveServer}
        onBackdropPress={setChangeServer}
        visible={showChangeServer}
      />
    </Fragment>
  );
};

Authentication.screenName = screens.AUTHENTICATION;
Authentication.options = () => {
  return {
    topBar: {
      visible: false,
      drawBehind: true,
      background: {
        color: '#02418b',
      },
      title: {
        color: '#fff',
      },
      backButton: {
        color: '#fff',
      },
    },
  };
};
export default observer(Authentication);
