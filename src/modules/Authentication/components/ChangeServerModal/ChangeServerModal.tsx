import React, { useState } from 'react';
import { View } from 'react-native';
import { Button, Text, Input, Overlay } from 'react-native-elements';
import RadioButton from '../../../../components/RadioButton';
import { useServerStore } from '../../../../stores/server.stores';
import { DetailServer } from '../../Authentication';

import styles from './ChangeServerModal.styles';

type Props = {
  visible: boolean;
  onBackdropPress: () => void;
  onSubmit: (detailServer: DetailServer) => void;
};
const ChangeServerModal: React.FC<Props> = ({
  visible,
  onBackdropPress,
  onSubmit,
}) => {
  const server = useServerStore();
  const [serverDomain, setServerDomain] = useState('');
  const [serverDatabase, setServerDatabase] = useState('');

  const [isSelected, setIsSelected] = useState([
    {
      id: 1,
      value: 'https',
      name: 'HTTPS',
      selected: server.getServer.protocol === 'https' || true,
    },
    {
      id: 2,
      value: 'http',
      name: 'HTTP',
      selected: server.getServer.protocol === 'http' || false,
    },
  ]);
  const onRadioBtnClick = (item: any) => () => {
    console.log(item);
    let updatedState = isSelected.map(isSelect =>
      isSelect.id === item.id
        ? { ...isSelect, selected: true }
        : { ...isSelect, selected: false },
    );
    setIsSelected(updatedState);
  };

  const handleSaveServer = () => {
    onSubmit({
      domain: serverDomain,
      database: serverDatabase,
      protocol: isSelected.find(item => item.selected)?.value || 'https',
    });
  };

  return (
    <Overlay isVisible={visible} onBackdropPress={onBackdropPress}>
      <View style={styles.container}>
        <Text style={styles.headerText}>URL Server</Text>
        <Input
          autoCompleteType="url"
          keyboardType="url"
          returnKeyType="next"
          placeholder="https://mydomain.com"
          onChangeText={setServerDomain}
          autoCapitalize="none"
          defaultValue={server.getServer.domain}
        />
        <Text style={styles.dialogImportantText}>
          Format penulisan URL seperti ini:
        </Text>
        <Text style={styles.dialogText}>
          Jika pakai IP Address: 10.24.25.46:8069
        </Text>
        <Text style={styles.dialogText}>
          Jika pakai domain: mydomain.com:8069
        </Text>
        <Text style={styles.dialogImportantText}>
          Jika tidak pakai port, hapus port-nya. Contoh: mydomain.com
        </Text>

        <Text style={styles.protokolText}>Protokol yang digunakan</Text>
        <View style={styles.radioContainer}>
          {isSelected.map(item => (
            <RadioButton
              onPress={onRadioBtnClick(item)}
              selected={item.selected}
              key={item.id}>
              {item.name}
            </RadioButton>
          ))}
        </View>

        <Input
          autoCompleteType
          label="Database"
          labelStyle={styles.databaseText}
          placeholder="mydatabase"
          onChangeText={setServerDatabase}
          autoCapitalize="none"
          defaultValue={server.getServer.database}
        />
      </View>

      <View style={styles.buttonContainer}>
        <Button
          type="clear"
          title="Batal"
          onPress={onBackdropPress}
          containerStyle={styles.button}
        />
        <Button type="clear" title="Simpan" onPress={handleSaveServer} />
      </View>
    </Overlay>
  );
};

export default ChangeServerModal;
