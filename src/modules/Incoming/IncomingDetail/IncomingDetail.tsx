import React, { Fragment, useEffect, useState } from 'react';
import { Alert, Pressable, View } from 'react-native';
import styles from './IncomingDetail.styles';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import Layout from 'components/Layout';
import { ScrollView } from 'react-native-gesture-handler';
import { Button, ListItem, Overlay, Text } from 'react-native-elements';
import ItemDetail from 'components/ItemDetail';
import useIncomingLine from 'modules/Incoming/hooks/useIncomingLine';
import useLoadingScreen from 'hooks/useLoadingScreen';
import Loading from 'components/Loading';

import get from 'lodash.get';
import odoo from 'services/odoo.client';
import ProductItem from '@components/ProductItem';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import state from 'helpers/state';
// import { useNavigation } from 'react-native-navigation-hooks/dist';
// import useIncomingList from 'modules/Incoming/hooks/useIncomingList';
import { Controller, useForm } from 'react-hook-form';
import { Calendar } from 'react-native-calendars';
import useToggle from 'hooks/useToggle';

type Props = IncomingData & {};
const IncomingDetail: ScreenFC<Props> = props => {
  // const { mutate } = useIncomingList();
  const { data: products = [], loading } = useIncomingLine(props.move_lines);

  // const navigation = useNavigation();
  // const [quantityDone, setQuantityDone] = useState<any[]>([]);
  const [showLoading, dismissLoading] = useLoadingScreen();

  const [transferDate, setTransferDate] = useState('');
  const [showValidate, toggleValidate] = useToggle();
  const [showExpiryDate, toggleExpiryDate] = useToggle();

  const { control, setValue, getValues, handleSubmit } = useForm({
    defaultValues: {
      quantity_done: 0,
      lot_name: '',
      expiry_date: '',
    },
  });

  useEffect(() => {
    setValue('quantity_done', products[0]?.quantity_done);
  }, [products, setValue]);

  useEffect(() => {
    (async () => {
      const moveLine = await odoo.rpc_call(
        'call_kw/stock.move.line/read',
        'stock.move.line',
        {
          args: [props.move_line_ids, ['lot_name', 'expiry_date']],
          method: 'read',
          kwargs: {},
        },
      );
      if (moveLine.error) {
        throw new Error(moveLine.error.data.message);
      }
      setValue('lot_name', moveLine?.result[0]?.lot_name);
      setValue('expiry_date', moveLine?.result[0]?.expiry_date);
    })();
  }, [setValue, props.move_line_ids]);

  const handleValidate = async () => {
    try {
      await showLoading();

      const res = await odoo.rpc_call(
        'call_kw/stock.picking.validate.manual.datetime.wizard/create',
        'stock.picking.validate.manual.datetime.wizard',
        {
          args: [{ data: transferDate, picking_id: props.id }],
          kwargs: {},
          method: 'create',
        },
      );

      // const res = await odoo.rpc_call('call_button', 'stock.picking', {
      //   method: 'button_validate',
      //   args: [[props.id]],
      // });

      if (res.error) {
        throw new Error(res.error.data.message);
      }

      toggleValidate();
      // mutate(prev => {
      //   return prev?.map(pages => {
      //     return pages.map(page => {
      //       if (page.id === props.id) {
      //         return {
      //           ...page,
      //           state: 'done',
      //         };
      //       }
      //       return page;
      //     });
      //   });
      // }, false);

      // navigation.updateProps({
      //   ...props,
      //   state: 'done',
      // });
      Alert.alert('Success', 'Berhasil menyetujui incoming');
    } catch (error: any) {
      console.log(error);
      Alert.alert('Error', error.message);
    } finally {
      dismissLoading();
    }
  };

  const handleSave = handleSubmit(async values => {
    try {
      await showLoading();
      const updatedData = [
        1,
        products[0].id,
        {
          quantity_done: Number(values.quantity_done),
        },
      ];

      const res = await odoo.rpc_call(
        'call_kw/stock.picking/write/',
        'stock.picking',
        {
          method: 'write',
          args: [
            [props.id],
            {
              move_lines: [updatedData],
            },
          ],
          kwargs: {},
        },
      );

      if (res.error) {
        throw new Error(res.error.data.message);
      }
      Alert.alert('Success', 'Berhasil menyimpan data');
    } catch (error: any) {
      console.log(error);
      Alert.alert('Error', error.message);
    } finally {
      dismissLoading();
    }
  });

  // const handleChangeQuantityDone = (productId: number) => (value: string) => {
  //   const newQuantityDone = [...quantityDone];
  //   const index = newQuantityDone.findIndex(q => q.id === productId);
  //   if (index !== -1) {
  //     newQuantityDone[index].quantity_done = Number(value);
  //   } else {
  //     newQuantityDone.push({
  //       id: productId,
  //       quantity_done: value,
  //     });
  //   }
  //   setQuantityDone(newQuantityDone);
  // };

  const handleGenerateNumber = async () => {
    try {
      const res = await odoo.rpc_call('call_button', 'stock.picking', {
        method: 'automatic_generate_lot_serial',
        args: [[props.id]],
      });
      if (res.error) {
        throw new Error(res.error.data.message);
      }

      const moveLine = await odoo.rpc_call(
        'call_kw/stock.move.line/read',
        'stock.move.line',
        {
          args: [props.move_line_ids, ['lot_name', 'expiry_date']],
          method: 'read',
          kwargs: {},
        },
      );

      if (moveLine.error) {
        throw new Error(moveLine.error.data.message);
      }
      setValue('lot_name', moveLine?.result[0]?.lot_name);
      Alert.alert('Success', 'Berhasil membuat nomor');
    } catch (error: any) {
      Alert.alert('Error', error.message);
    }
  };

  return (
    <Layout>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            {props.name}
          </Text>
        </View>

        <View style={styles.detail}>
          <ItemDetail title="Status" value={state[props.state]} />
          <ItemDetail title="Partner" value={props.partner_id} />
          <ItemDetail title="Source Location" value={props.location_id} />
          <ItemDetail title="Nama Gudang" value={props.picking_type_id} />
          <ItemDetail
            title="Destination Location"
            value={props.location_dest_id}
          />
          <ItemDetail title="Scheduled Date" value={props.scheduled_date} />
          <ItemDetail title="NO PO" value={props.origin} />
          <ItemDetail title="Ref. No Faks" value={props.ref_fax} />
        </View>

        {products.length > 0 && (
          <Fragment>
            <View style={styles.header}>
              <Text h4 style={styles.headerText}>
                Products
              </Text>
            </View>
            <View style={styles.detail}>
              {products.map(product => (
                <Fragment key={product.id}>
                  <ListItem hasTVPreferredFocus tvParallaxProperties>
                    <ListItem.Content>
                      <ListItem.Title>
                        {get(product, 'product_id.1', '') || ''}
                      </ListItem.Title>
                    </ListItem.Content>
                  </ListItem>

                  <ProductItem
                    label="Quantity"
                    value={product?.product_uom_qty}
                  />
                  <ProductItem label="JT 1" value={product?.jt1} />
                  <ProductItem label="JT 2" value={product?.jt2} />
                  <ProductItem label="JT Bruto" value={product?.jtnetto} />
                  <ProductItem
                    label="Digital/Analog"
                    value={product?.digital_analog}
                  />

                  <Controller
                    control={control}
                    name="lot_name"
                    render={({ field }) => (
                      <Fragment>
                        {field.value === '' && (
                          <Button
                            title="Generate No"
                            onPress={handleGenerateNumber}
                          />
                        )}
                        <ProductItem
                          label="Lot Number"
                          value={field.value || ''}
                          editable={false}
                          onChangeText={field.onChange}
                          multiline
                        />
                      </Fragment>
                    )}
                  />
                  <Controller
                    control={control}
                    name="expiry_date"
                    render={({ field }) => (
                      <Pressable onPress={toggleExpiryDate}>
                        <ProductItem
                          label="Expiry Date"
                          value={field.value || ''}
                          editable={false}
                          onChangeText={field.onChange}
                        />
                      </Pressable>
                    )}
                  />

                  <Controller
                    control={control}
                    name="quantity_done"
                    render={({ field }) => (
                      <ProductItem
                        label="Done"
                        value={field.value}
                        editable
                        onChangeText={field.onChange}
                        keyboardType="numeric"
                      />
                    )}
                  />
                  {/* <ProductItem
                    label="Done"
                    value={product?.quantity_done}
                    editable
                    onChangeText={handleChangeQuantityDone(product.id)}
                    keyboardType="numeric"
                  /> */}
                </Fragment>
              ))}
            </View>
          </Fragment>
        )}

        {props.state !== 'done' && (
          <Fragment>
            <Button
              title={'Save'}
              onPress={handleSave}
              containerStyle={{
                marginBottom: heightPercentageToDP(2),
              }}
            />
            <Button
              title={'Validate'}
              // onPress={handleValidate}
              onPress={toggleValidate}
              disabled={props.state === 'done'}
            />
          </Fragment>
        )}
      </ScrollView>

      <Loading isVisible={loading} />

      <Overlay isVisible={showValidate} onBackdropPress={toggleValidate}>
        <Calendar
          minDate={'2012-05-10'}
          onDayPress={day => {
            setTransferDate(day.dateString);
          }}
          monthFormat={'yyyy MM'}
          disableAllTouchEventsForDisabledDays={true}
          enableSwipeMonths={true}
          markedDates={{
            [transferDate]: {
              selected: true,
              disableTouchEvent: true,
            },
          }}
        />
        <Button title={'Validate'} onPress={handleValidate} />
      </Overlay>

      <Overlay isVisible={showExpiryDate} onBackdropPress={toggleExpiryDate}>
        <Calendar
          minDate={'2012-05-10'}
          onDayPress={async day => {
            try {
              setValue('expiry_date', day.dateString);
              toggleExpiryDate();

              const res = await odoo.rpc_call(
                'call_kw/stock.move/write',
                'stock.move',
                {
                  method: 'write',
                  args: [
                    props.move_lines,
                    {
                      move_line_ids: [
                        [
                          1,
                          props.move_line_ids[0],
                          {
                            expiry_date: day.dateString,
                          },
                        ],
                      ],
                    },
                  ],
                  kwargs: {},
                },
              );

              if (res.error) {
                throw new Error(res.error.data.message);
              }

              console.log(res);
            } catch (error: any) {
              Alert.alert('Error', error.message);
            }
          }}
          markedDates={{
            [getValues('expiry_date')]: {
              selected: true,
              disableTouchEvent: true,
            },
          }}
          monthFormat={'yyyy MM'}
          disableAllTouchEventsForDisabledDays={true}
          enableSwipeMonths={true}
        />
      </Overlay>
    </Layout>
  );
};

IncomingDetail.screenName = screens.INCOMING_DETAIL;
IncomingDetail.options = props => {
  return {
    topBar: {
      title: {
        text: props.name,
      },
    },
  };
};

export default IncomingDetail;
