import Layout from 'components/Layout';
import React from 'react';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './IncomingList.styles';
import { useNavigation } from 'react-native-navigation-hooks';

import IncomingItem from '../components/IncomingItem';
import useIncomingList from 'modules/Incoming/hooks/useIncomingList';
import Loading from 'components/Loading';
import { FlatList } from 'react-native';
import FlatListEmpty from 'components/FlatListEmpty';
import { SearchBar } from 'react-native-elements';

type Props = {};
const IncomingList: ScreenFC<Props> = () => {
  const navigation = useNavigation();

  const [searchText, setSearchText] = React.useState('');

  const [searchedText, setSearchedText] = React.useState('');
  const { data, loading, setSize, size } = useIncomingList(searchedText);

  const handleGoToDetail = (item: IncomingData) => () => {
    navigation.push(screens.INCOMING_DETAIL, item);
  };
  const renderItem = ({ item }: { item: IncomingData }) => {
    return <IncomingItem {...item} onPress={handleGoToDetail(item)} />;
  };
  const keyExtractor = (item: IncomingData) => `${item.id}`;

  const onEndReached = () => {
    setSize(size + 1);
  };

  const handleOnSubmitEditing = () => {
    setSearchedText(searchText);
  };
  const handleClearText = () => {
    setSearchText('');
  };

  // const handleRevalidate = () => {
  //   mutate(prev => {
  //     return prev?.map(p => {
  //       return p.map(e => ({
  //         ...e,
  //         state: 'draft',
  //       }));
  //     });
  //   }, false);
  // };
  return (
    <Layout>
      <SearchBar
        autoFocus
        autoCapitalize="none"
        placeholder="Type Here..."
        lightTheme
        round={false}
        platform="android"
        // onChangeText={this.updateSearch}
        value={searchText}
        showLoading={false}
        loadingProps={{ size: 'small' }}
        autoCompleteType
        onBlur={() => {}}
        onFocus={() => {}}
        onChangeText={setSearchText as any}
        onClear={handleClearText}
        cancelIcon={{
          type: 'ionicon',
          name: 'arrow-back',
        }}
        searchIcon={{
          type: 'ionicon',
          name: 'search',
        }}
        clearIcon={{
          type: 'ionicon',
          name: 'close',
        }}
        leftIcon={{
          type: 'ionicon',
          name: 'search',
        }}
        onCancel={() => {}}
        cancelButtonTitle="Batal"
        cancelButtonProps={{}}
        showCancel
        onSubmitEditing={handleOnSubmitEditing}
      />

      <FlatList
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        data={data.flat(1)}
        contentContainerStyle={styles.IncomingListWrapper}
        alwaysBounceVertical
        scrollEventThrottle={16}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.5}
        ListEmptyComponent={FlatListEmpty}
      />
      <Loading isVisible={loading} />
    </Layout>
  );
};

IncomingList.screenName = screens.INCOMING_LIST;
IncomingList.options = () => ({
  topBar: {
    title: {
      text: 'Incoming',
    },
  },
});
export default IncomingList;
