import React from 'react';
import { View } from 'react-native';
import styles from './IncomingItem.styles';

import DropShadow from 'react-native-drop-shadow';
import { Divider, ListItem, Text } from 'react-native-elements';
import state from 'helpers/state';

type ItemProps = {
  title: string;
  value: string | number | [number, string] | boolean;
};
const Item: React.FC<ItemProps> = ({ title, value }) => (
  <View style={styles.itemContainer}>
    <ListItem.Subtitle style={styles.itemLeftText}>{title}</ListItem.Subtitle>
    <ListItem.Title style={styles.itemRightText}>
      {Array.isArray(value) ? value[1] : value}
    </ListItem.Title>
  </View>
);

type IncomingItemProps = IncomingData & {
  onPress?: () => void;
};
const IncomingItem: React.FC<IncomingItemProps> = ({ onPress, ...props }) => {
  return (
    <DropShadow style={styles.container}>
      <ListItem hasTVPreferredFocus tvParallaxProperties onPress={onPress}>
        <ListItem.Content>
          <View style={styles.header}>
            <Text style={styles.headerTextSurat}>{props.name}</Text>
            <Text style={styles.headerTextDate}>{state[props.state]}</Text>
          </View>
          <Divider style={styles.divider} />

          <Item title="Source Location" value={props.location_id} />
          <Item title="Destination Location" value={props.location_dest_id} />
          <Item title="Scheduled Date" value={props.scheduled_date} />
          <Item title="Ref. No Faks" value={props.ref_fax} />
        </ListItem.Content>
      </ListItem>
    </DropShadow>
  );
};
export default IncomingItem;
