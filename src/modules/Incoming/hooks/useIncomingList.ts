import { getIncomingList } from 'modules/Incoming/services';
import useSWRInfinite from 'swr/infinite';

const PAGE_SIZE = 10;
const getKey =
  (search: string) => (pageIndex: number, previousPageData: any) => {
    if (previousPageData && !previousPageData.length) {
      return null;
    } // reached the end

    return `/api/get-incoming-list?page=${pageIndex}&limit=10&offset=${
      pageIndex * PAGE_SIZE
    }&search=${search}`;
  };
const useIncomingList = (searchText = '') => {
  const { data, size, setSize, error, ...rest } = useSWRInfinite(
    getKey(searchText),
    getIncomingList,
  );

  const isLoadingInitialData = !data && !error;
  const isLoadingMore =
    isLoadingInitialData ||
    (size > 0 && data && typeof data[size - 1] === 'undefined');
  const isEmpty = data?.[0]?.length === 0;
  const isReachingEnd =
    isEmpty || (data && data[data.length - 1]?.length < PAGE_SIZE);

  return {
    data: data || [],
    loading: !!isLoadingMore && !isReachingEnd,
    size,
    setSize,
    ...rest,
  };
};
export default useIncomingList;
