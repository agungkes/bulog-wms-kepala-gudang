import odoo from 'services/odoo.client';

const getIncomingLine = (stockPickingMoveId: number | number[]) => async () => {
  const res = await odoo.get<IncomingMoving[]>('stock.move', {
    args: [
      stockPickingMoveId,
      [
        'name',
        'picking_type_id',
        'origin_returned_move_id',
        'date_expected',
        'state',
        'location_id',
        'location_dest_id',
        'scrapped',
        'picking_code',
        'product_type',
        'show_details_visible',
        'show_reserved_availability',
        'show_operations',
        'additional',
        'has_move_lines',
        'is_locked',
        'product_id',
        'analytic_account_id',
        'branch_id',
        'is_initial_demand_editable',
        'is_quantity_done_editable',
        'product_uom_qty',
        'qty_putaway',
        'qty_picking',
        'reserved_availability',
        'jt1',
        'date_jt1',
        'jt2',
        'date_jt2',
        'jtnetto',
        'digital_analog',
        'sec_uom_analog',
        'quantity_done',
        'product_uom',
        'sh_is_secondary_unit',
        'sh_sec_qty',
        'sh_sec_done_qty',
        'sh_sec_uom',
        'qc_status',
      ],
    ],
  });

  return res;
};

export default getIncomingLine;
