// import getStockPickingType from 'services/getStockPickingType';
import odoo from 'services/odoo.client';

const getIncomingList = async (props: string) => {
  const index = props.lastIndexOf('?');
  const searchString = props.slice(index + 1);
  const pagination = JSON.parse(
    '{"' +
      decodeURI(searchString)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/[=]/g, '":"') +
      '"}',
  );

  const data = await odoo.search_read<IncomingData[]>('stock.picking', {
    context: { lang: 'en_US', tz: 'Asia/Jakarta' },
    domain: [
      ['jenis_operasi', '=', 'incoming'],
      ['name', 'ilike', pagination.search],
    ] as any,
    fields: [
      'name',
      'location_id',
      'location_dest_id',
      'partner_id',
      'date',
      'scheduled_date',
      'origin',
      'ref_fax',
      'group_id',
      'backorder_id',
      'state',
      'state_inv',
      'invoice_paid',
      'priority',
      'picking_type_id',
      'batch_id',
      'move_lines',
      'move_line_ids',
      'jenis_operasi',
    ],
    sort: 'id DESC',
    limit: Number(pagination.limit),
    offset: Number(pagination.offset),
  });

  return data;
};

export default getIncomingList;
