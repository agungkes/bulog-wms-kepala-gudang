import React from 'react';
import { ActivityIndicator, Image, View } from 'react-native';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './Loading.styles';

const Loading: ScreenFC = () => {
  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image
          source={require('../../assets/images/logo.png')}
          resizeMode="contain"
          style={styles.logo}
        />
        <ActivityIndicator color={'#e97e11'} size={'large'} />
      </View>
    </View>
  );
};

Loading.screenName = screens.LOADING;
export default Loading;
