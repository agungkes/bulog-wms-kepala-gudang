import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  NotificationWrapper: {
    padding: widthPercentageToDP(4),
  },
  item: {
    marginBottom: heightPercentageToDP(1),
    backgroundColor: '#fff',
  },
  itemContainer: {
    shadowColor: '#999999',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    marginBottom: heightPercentageToDP(2),
  },
});
export default styles;
