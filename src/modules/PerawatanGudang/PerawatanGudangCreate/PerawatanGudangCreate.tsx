import React from 'react';
import Layout from 'components/Layout';
import { Text } from 'react-native-elements';
import styles from './PerawatanGudangCreate.styles';
import { ScrollView } from 'react-native-gesture-handler';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';

const PerawatanGudangCreate: ScreenFC = () => {
  return (
    <Layout>
      <ScrollView contentContainerStyle={styles.container}>
        <Text>Hm</Text>
      </ScrollView>
    </Layout>
  );
};
PerawatanGudangCreate.screenName = screens.PERAWATAN_GUDANG_ADD;
PerawatanGudangCreate.options = () => {
  return {
    topBar: {
      title: {
        text: 'Create Perawatan Gudang',
      },
    },
  };
};
export default PerawatanGudangCreate;
