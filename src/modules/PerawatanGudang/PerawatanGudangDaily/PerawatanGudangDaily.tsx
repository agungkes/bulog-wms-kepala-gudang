import React from 'react';
import Layout from 'components/Layout';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import useDailyMonitoring from 'modules/PerawatanGudang/hooks/useDailyMonitoring';
import { FlatList } from 'react-native-gesture-handler';
import {
  useNavigation,
  useNavigationButtonPress,
} from 'react-native-navigation-hooks/dist';
import DailyItem from 'modules/PerawatanGudang/components/DailyItem';
import styles from './PerawatanGudangDaily.styles';
import FlatListEmpty from 'components/FlatListEmpty';
import Loading from 'components/Loading';
import Icon from 'react-native-vector-icons/Ionicons';

const AddPerawatanGudangIcon = Icon.getImageSourceSync('md-add', 30, 'white');
const PerawatanGudangDaily: ScreenFC = () => {
  const { setSize, data, size, isLoadingMore, isReachingEnd } =
    useDailyMonitoring();

  const navigation = useNavigation();
  const handleGoToCreateDaily = () => {
    navigation.push(screens.PERAWATAN_GUDANG_DAILY_CREATE);
  };
  useNavigationButtonPress(handleGoToCreateDaily);

  const handleGoToDetail = (item: GetDailyMonitoring) => () => {
    navigation.push(screens.PERAWATAN_GUDANG_DAILY_DETAIL, item);
  };
  const renderItem = ({ item }: { item: GetDailyMonitoring }) => {
    return <DailyItem {...item} onPress={handleGoToDetail(item)} />;
  };
  const keyExtractor = (item: GetDailyMonitoring) => `${item.id}`;

  const onEndReached = () => {
    setSize(size + 1);
  };

  return (
    <Layout>
      <FlatList
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        data={data.flat(1)}
        contentContainerStyle={styles.container}
        alwaysBounceVertical
        scrollEventThrottle={16}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.5}
        ListEmptyComponent={FlatListEmpty}
      />

      <Loading isVisible={!!isLoadingMore && !isReachingEnd} />
    </Layout>
  );
};
PerawatanGudangDaily.screenName = screens.PERAWATAN_GUDANG_DAILY_LIST;
PerawatanGudangDaily.options = () => {
  return {
    topBar: {
      title: {
        text: 'Perawatan Gudang Daily',
      },
    },
    fab: {
      id: 'ADD_PERAWATAN_GUDANG',
      iconColor: '#fff',
      clickColor: '#e97e11',
      backgroundColor: '#02418b',
      icon: AddPerawatanGudangIcon,
    },
  };
};

export default PerawatanGudangDaily;
