import React, { useEffect } from 'react';
import Layout from 'components/Layout';
import { Button, ListItem } from 'react-native-elements';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import { ScrollView } from 'react-native-gesture-handler';

import styles from './PerawatanGudangDailyCreate.styles';
import useQuestion from 'modules/PerawatanGudang/hooks/useQuestion';
import QuestionItem from 'modules/PerawatanGudang/components/QuestionItem';
import Loading from 'components/Loading';
import useDailyDefaultMonitoring from 'modules/PerawatanGudang/hooks/useDailyDefaultMonitoring';
import useWarehouseByName from 'modules/PerawatanGudang/hooks/useWarehouseByName';
import useCompany from 'modules/PerawatanGudang/hooks/useCompany';
import { Picker } from '@react-native-picker/picker';
import useDestinationLocation from 'modules/Putaway/hooks/useDestinationLocation';
import { Controller, FormProvider, useForm } from 'react-hook-form';
import { Alert } from 'react-native';
import odoo from 'services/odoo.client';
import useLoadingScreen from 'hooks/useLoadingScreen';
import { useNavigation } from 'react-native-navigation-hooks/dist';

const mappedLine = (e: any) => [
  0,
  0,
  {
    result: e.result,
    result1: e.result1,
    qna_id: e.qna_id[0],
    type: e.type,
  },
];
const PerawatanGudangDailyCreate: ScreenFC = () => {
  const navigation = useNavigation();
  const methods = useForm<any>({
    defaultValues: {
      kualitas_awal_line: [],
      sanitasi_gudang_ids: [],
      aerasi_line_ids: [],
      pengosongan_gudang_ids: [],
      warehouse: '',
      unit: '',
      company_id: '',
      kepala_gudang_id: '',
      location_id: '',
      warehouse_id: '',
    },
  });

  const [showLoading, dismissLoading] = useLoadingScreen();
  const { data: pemeriksaanAwal, loading: isPemeriksaanAwalLoading } =
    useQuestion({
      model: 'vit.kualitas_awal_line',
      ids: [7, 8, 9],
    });

  const { data: sanitasiGudang, loading: isSanitasiLoading } = useQuestion({
    model: 'vit.sanitasi_gudang_line',
    ids: [
      43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
      61, 62, 63,
    ],
  });

  const { data: aerasi, loading: isAerasiLoading } = useQuestion({
    model: 'vit.aerasi_gudang_line',
    ids: [196, 197, 198, 199, 200, 201, 202],
  });
  const { data: warehouses, loading: isWarehouseLoading } = useQuestion({
    model: 'vit.pengosongan_gudang_line',
    ids: [82, 83, 84],
  });

  const { data: defaultData } = useDailyDefaultMonitoring();
  const { name: warehouseName } = useWarehouseByName(defaultData?.warehouse_id);
  const { name: companyName } = useCompany(defaultData?.company_id);
  const { data: warehouse } = useDestinationLocation('');

  useEffect(() => {
    methods.setValue('kualitas_awal_line', pemeriksaanAwal);
    methods.setValue('sanitasi_gudang_ids', sanitasiGudang);
    methods.setValue('aerasi_line_ids', aerasi);
    methods.setValue('pengosongan_gudang_ids', warehouses);
  }, [pemeriksaanAwal, sanitasiGudang, aerasi, warehouses, methods]);

  useEffect(() => {
    methods.setValue('company_id', defaultData?.company_id);
    methods.setValue('warehouse_id', defaultData?.warehouse_id);
    methods.setValue('kepala_gudang_id', defaultData?.kepala_gudang_id);
  }, [defaultData, methods]);

  const handleSave = methods.handleSubmit(async values => {
    try {
      await showLoading();
      const data = {
        company_id: values.company_id,
        warehouse_id: values.warehouse_id,
        kepala_gudang_id: values.kepala_gudang_id,
        location_id: values.location_id,
        aerasi_line_ids: values.aerasi_line_ids.map(mappedLine),
        kualitas_awal_ids: values.kualitas_awal_line.map(mappedLine),
        pengosongan_gudang_ids: values.pengosongan_gudang_ids.map(mappedLine),
        sanitasi_gudang_ids: values.sanitasi_gudang_ids.map(mappedLine),
      };

      const res = await odoo.rpc_call(
        'call_kw/vit.daily_monitoring/create',
        'vit.daily_monitoring',
        {
          method: 'create',
          args: [data],
          kwargs: {},
        },
      );

      if (res.error) {
        throw new Error(res.error.data.message);
      }

      Alert.alert('Success', 'Data berhasil disimpan', [
        {
          text: 'OK',
          onPress: () => {
            navigation.pop();
          },
        },
      ]);
    } catch (error: any) {
      console.log(error);
      Alert.alert('Error', error.message);
    } finally {
      dismissLoading();
    }
  });
  return (
    <FormProvider {...methods}>
      <Layout>
        <ScrollView contentContainerStyle={styles.container}>
          <ListItem tvParallaxProperties hasTVPreferredFocus>
            <ListItem.Content>
              <ListItem.Title>Gudang</ListItem.Title>
            </ListItem.Content>
            <Controller
              name="warehouse"
              control={methods.control}
              defaultValue={warehouseName}
              render={({ field: { onChange, onBlur } }) => (
                <ListItem.Input
                  placeholder="Pilih gudang"
                  autoCompleteType
                  autoFocus
                  editable={false}
                  multiline
                  onChangeText={onChange}
                  onBlur={onBlur}
                  value={warehouseName}
                />
              )}
            />
          </ListItem>

          <ListItem tvParallaxProperties hasTVPreferredFocus>
            <ListItem.Content>
              <ListItem.Title>Unit Gudang</ListItem.Title>
            </ListItem.Content>
          </ListItem>
          <Controller
            name="location_id"
            control={methods.control}
            render={({ field: { onChange, value } }) => (
              <Picker selectedValue={value} onValueChange={onChange}>
                {warehouse.map(w => (
                  <Picker.Item key={w[0]} label={w[1]} value={w[0]} />
                ))}
              </Picker>
            )}
          />

          <ListItem tvParallaxProperties hasTVPreferredFocus>
            <ListItem.Content>
              <ListItem.Title>Company</ListItem.Title>
            </ListItem.Content>
            <ListItem.Input
              autoCompleteType
              editable={false}
              defaultValue={`${companyName || ''}`}
            />
          </ListItem>
          <ListItem tvParallaxProperties hasTVPreferredFocus>
            <ListItem.Content>
              <ListItem.Title>Tanggal</ListItem.Title>
            </ListItem.Content>
            <ListItem.Input
              placeholder=""
              defaultValue={`${defaultData?.date || ''}`}
              autoCompleteType
            />
          </ListItem>

          <QuestionItem
            title="Pemeriksaan Kualitas Awal"
            questions={pemeriksaanAwal || []}
            name="kualitas_awal_line"
          />
          <QuestionItem
            title="Sanitasi Gudang dan Lingkungan"
            name="sanitasi_gudang_ids"
            questions={sanitasiGudang || []}
          />
          <QuestionItem
            title="Aerasi Gudang"
            questions={aerasi || []}
            name="aerasi_line_ids"
          />
          <QuestionItem
            name="pengosongan_gudang_ids"
            title="Pengosongan Gudang"
            questions={warehouses || []}
          />

          <Button title={'Save'} onPress={handleSave} />
        </ScrollView>

        <Loading
          isVisible={
            isPemeriksaanAwalLoading ||
            isSanitasiLoading ||
            isAerasiLoading ||
            isWarehouseLoading
          }
        />
      </Layout>
    </FormProvider>
  );
};
PerawatanGudangDailyCreate.screenName = screens.PERAWATAN_GUDANG_DAILY_CREATE;
PerawatanGudangDailyCreate.options = () => {
  return {
    topBar: {
      title: {
        text: 'Perawatan Gudang Daily',
      },
    },
  };
};
export default PerawatanGudangDailyCreate;
