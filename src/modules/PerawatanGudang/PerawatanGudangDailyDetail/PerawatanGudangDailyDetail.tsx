import React, { Fragment, useEffect } from 'react';
import { Alert, View } from 'react-native';
import { Button, Text } from 'react-native-elements';
import styles from './PerawatanGudangDailyDetail.styles';
import Layout from '@components/Layout';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import { ScrollView } from 'react-native-gesture-handler';
import ItemDetail from 'components/ItemDetail';
import Loading from 'components/Loading';
import useQuestion from 'modules/PerawatanGudang/hooks/useQuestion';
import QuestionItem from 'modules/PerawatanGudang/components/QuestionItem';
import { FormProvider, useForm } from 'react-hook-form';
import useDailyDefaultMonitoring from 'modules/PerawatanGudang/hooks/useDailyDefaultMonitoring';
import useLoadingScreen from 'hooks/useLoadingScreen';
import odoo from 'services/odoo.client';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import useDailyMonitoring from 'modules/PerawatanGudang/hooks/useDailyMonitoring';
import state from 'helpers/state';
import { useNavigation } from 'react-native-navigation-hooks/dist';

const mappedLine = (e: any) => [
  1,
  e.id,
  {
    result: e.result,
    result1: e.result1,
    qna_id: e.qna_id[0],
    type: e.type,
  },
];
const PerawatanGudangDailyDetail: ScreenFC<GetDailyMonitoring> = props => {
  const { mutate } = useDailyMonitoring();
  const navigation = useNavigation();
  const { data: pemeriksaanAwal, loading: isPemeriksaanAwalLoading } =
    useQuestion({
      model: 'vit.kualitas_awal_line',
      ids: props.kualitas_awal_ids,
    });

  const [showLoading, dismissLoading] = useLoadingScreen();
  const { data: sanitasiGudang, loading: isSanitasiLoading } = useQuestion({
    model: 'vit.sanitasi_gudang_line',
    ids: props.sanitasi_gudang_ids,
  });

  const { data: aerasi, loading: isAerasiLoading } = useQuestion({
    model: 'vit.aerasi_gudang_line',
    ids: props.aerasi_line_ids,
  });
  const { data: warehouses, loading: isWarehouseLoading } = useQuestion({
    model: 'vit.pengosongan_gudang_line',
    ids: props.pengosongan_gudang_ids,
  });
  const { data: defaultData } = useDailyDefaultMonitoring();

  const methods = useForm<any>({
    defaultValues: {
      kualitas_awal_line: [],
      sanitasi_gudang_ids: [],
      aerasi_line_ids: [],
      pengosongan_gudang_ids: [],
      warehouse: '',
      unit: '',
      company_id: '',
      kepala_gudang_id: '',
      location_id: '',
      warehouse_id: '',
    },
  });

  useEffect(() => {
    methods.setValue('kualitas_awal_line', pemeriksaanAwal);
    methods.setValue('sanitasi_gudang_ids', sanitasiGudang);
    methods.setValue('aerasi_line_ids', aerasi);
    methods.setValue('pengosongan_gudang_ids', warehouses);
  }, [pemeriksaanAwal, sanitasiGudang, aerasi, warehouses, methods]);

  useEffect(() => {
    methods.setValue('company_id', defaultData?.company_id);
    methods.setValue('warehouse_id', defaultData?.warehouse_id);
    methods.setValue('kepala_gudang_id', defaultData?.kepala_gudang_id);
    methods.setValue('location_id', props.location_id[0]);
  }, [defaultData, methods, props.location_id]);

  const handleSave = methods.handleSubmit(async values => {
    try {
      await showLoading();
      const data = {
        company_id: values.company_id,
        warehouse_id: values.warehouse_id,
        kepala_gudang_id: values.kepala_gudang_id,
        location_id: values.location_id,
        aerasi_line_ids: values.aerasi_line_ids.map(mappedLine),
        kualitas_awal_ids: values.kualitas_awal_line.map(mappedLine),
        pengosongan_gudang_ids: values.pengosongan_gudang_ids.map(mappedLine),
        sanitasi_gudang_ids: values.sanitasi_gudang_ids.map(mappedLine),
      };

      const res = await odoo.rpc_call(
        'call_kw/vit.daily_monitoring/write',
        'vit.daily_monitoring',
        {
          method: 'write',
          args: [props.id, data],
          kwargs: {},
        },
      );

      if (res.error) {
        throw new Error(res.error.data.message);
      }

      mutate();
      Alert.alert('Success', 'Data berhasil disimpan');
    } catch (error: any) {
      console.log(error);
      Alert.alert('Error', error.message);
    } finally {
      dismissLoading();
    }
  });

  const handleValidate = async () => {
    try {
      await showLoading();
      const res = await odoo.rpc_call('call_button', 'vit.daily_monitoring', {
        method: 'action_confirm', // 'action_done',
        args: [props.id],
      });

      if (res.error) {
        throw new Error(res.error.data.message);
      }

      mutate(prev => {
        return prev?.map(p => {
          return p.map(c => {
            if (c.id === props.id) {
              return {
                ...c,
                state: 'approve',
              };
            }

            return c;
          });
        });
      }, false);

      navigation.updateProps({
        ...props,
        state: 'approve',
      });
      Alert.alert('Success', 'Berhasil approve');
    } catch (error: any) {
      Alert.alert('Error', error.message);
    } finally {
      dismissLoading();
    }
  };
  return (
    <FormProvider {...methods}>
      <Layout>
        <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.header}>
            <Text h4 style={styles.headerText}>
              {props.name}
            </Text>
          </View>

          <View style={styles.detail}>
            <ItemDetail title="State" value={state[props.state]} />
            <ItemDetail title="Kepala Gudang" value={props.kepala_gudang_id} />
            <ItemDetail title="Gudang" value={props.warehouse_id} />
            <ItemDetail title="Unit Gudang" value={props.location_id} />
            <ItemDetail title="Date" value={props.date} />
          </View>

          <QuestionItem
            title="Pemeriksaan Kualitas Awal"
            questions={pemeriksaanAwal || []}
            name="kualitas_awal_line"
            disabled={props.state !== 'draft'}
          />
          <QuestionItem
            title="Sanitasi Gudang dan Lingkungan"
            name="sanitasi_gudang_ids"
            questions={sanitasiGudang || []}
            disabled={props.state !== 'draft'}
          />
          <QuestionItem
            title="Aerasi Gudang"
            questions={aerasi || []}
            name="aerasi_line_ids"
            disabled={props.state !== 'draft'}
          />
          <QuestionItem
            name="pengosongan_gudang_ids"
            title="Pengosongan Gudang"
            questions={warehouses || []}
            disabled={props.state !== 'draft'}
          />

          {props.state === 'draft' && (
            <Fragment>
              <Button
                title={'Save'}
                onPress={handleSave}
                containerStyle={{
                  marginBottom: heightPercentageToDP(1),
                }}
              />
              <Button title={'Approve'} onPress={handleValidate} />
            </Fragment>
          )}
        </ScrollView>

        <Loading
          isVisible={
            isPemeriksaanAwalLoading ||
            isSanitasiLoading ||
            isAerasiLoading ||
            isWarehouseLoading
          }
        />
      </Layout>
    </FormProvider>
  );
};

PerawatanGudangDailyDetail.screenName = screens.PERAWATAN_GUDANG_DAILY_DETAIL;
PerawatanGudangDailyDetail.options = () => {
  return {
    topBar: {
      title: {
        text: 'Detail Perawatan Gudang',
      },
    },
  };
};
export default PerawatanGudangDailyDetail;
