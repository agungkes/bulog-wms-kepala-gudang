import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import styles from './PerawatanGudangDetail.styles';
import Layout from '@components/Layout';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';

const PerawatanGudangDetail: ScreenFC = () => {
  return (
    <Layout>
      <View style={styles.container}>
        <Text>HAI DARI PERAWATAN GUDANG DETAIL</Text>
      </View>
    </Layout>
  );
};
PerawatanGudangDetail.screenName = screens.PERAWATAN_GUDANG_DETAIL;
PerawatanGudangDetail.options = () => {
  return {
    topBar: {
      title: {
        text: 'Perawatan Gudang Detail',
      },
    },
  };
};
export default PerawatanGudangDetail;
