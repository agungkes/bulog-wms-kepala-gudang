import React from 'react';
import Layout from 'components/Layout';
import { ListItem } from 'react-native-elements';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import { useNavigation } from 'react-native-navigation-hooks';

const PerawatanGudangList: ScreenFC = () => {
  const navigation = useNavigation();
  const handleGoToDaily = () => {
    navigation.push(screens.PERAWATAN_GUDANG_DAILY_LIST);
  };
  return (
    <Layout>
      <ListItem
        hasTVPreferredFocus
        tvParallaxProperties
        onPress={handleGoToDaily}>
        <ListItem.Content>
          <ListItem.Title>Daily Monitoring</ListItem.Title>
        </ListItem.Content>
      </ListItem>
      <ListItem hasTVPreferredFocus tvParallaxProperties>
        <ListItem.Content>
          <ListItem.Title>Weekly Monitoring</ListItem.Title>
        </ListItem.Content>
      </ListItem>
    </Layout>
  );
};

PerawatanGudangList.screenName = screens.PERAWATAN_GUDANG_LIST;
PerawatanGudangList.options = () => {
  return {
    topBar: {
      title: {
        text: 'Perawatan Gudang',
      },
    },
  };
};
export default PerawatanGudangList;
