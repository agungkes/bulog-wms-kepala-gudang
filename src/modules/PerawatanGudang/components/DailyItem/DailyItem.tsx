import React from 'react';
import { View } from 'react-native';
import styles from './DailyItem.styles';

import DropShadow from 'react-native-drop-shadow';
import { Divider, ListItem, Text } from 'react-native-elements';
import Item from 'components/Item';
import state from 'helpers/state';

type Props = GetDailyMonitoring & {
  onPress?: () => void;
};
const DailyItem: React.FC<Props> = ({ onPress, ...props }) => {
  return (
    <DropShadow style={styles.container}>
      <ListItem hasTVPreferredFocus tvParallaxProperties onPress={onPress}>
        <ListItem.Content>
          <View style={styles.header}>
            <Text style={styles.headerTextSurat}>{props.name}</Text>
            <Text style={styles.headerTextDate}>{state[props.state]}</Text>
          </View>
          <Divider style={styles.divider} />

          <Item title="Gudang" value={props.gudang_id} />
          <Item title="Kepala Gudang" value={props.kepala_gudang_id} />
          <Item title="Date" value={props.date} />
        </ListItem.Content>
      </ListItem>
    </DropShadow>
  );
};
export default DailyItem;
