import { StyleSheet } from 'react-native';
import { heightPercentageToDP } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
  },
  headerText: {
    textAlign: 'center',
  },

  detail: {
    marginVertical: heightPercentageToDP(1),
  },
});
export default styles;
