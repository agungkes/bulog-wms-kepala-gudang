import React, { Fragment, useEffect, useState } from 'react';
import { Controller, useFieldArray, useFormContext } from 'react-hook-form';
import { GestureResponderEvent, View } from 'react-native';
import { ListItem, Text, useTheme } from 'react-native-elements';
import styles from './QuestionItem.styles';

type QuestionItemProps = {
  title: string;
  questions?: QuestionResponse[];
  onPress?: (id: number) => void;
  name: string;
  disabled?: boolean;
};
const QuestionItem: React.FC<QuestionItemProps> = ({
  title,
  questions,
  name,
  disabled = false,
}) => {
  const { theme } = useTheme();
  const { control, setValue } = useFormContext();
  const { fields } = useFieldArray({
    control,
    name,
  });

  const [alteredQuestion, setAlteredQuestion] = useState<
    QuestionResponse[] | undefined
  >(questions);

  useEffect(() => {
    setAlteredQuestion(questions);
  }, [questions]);
  const handleSelect =
    (item: QuestionResponse) => (_event: GestureResponderEvent) => {
      const newAlteredQuestion = [...(alteredQuestion || [])];
      const index = newAlteredQuestion.findIndex(f => f.id === item.id);
      if (index !== -1) {
        Object.assign(newAlteredQuestion[index], {
          result: !newAlteredQuestion[index].result,
        });
      }
      setValue(name, newAlteredQuestion);
      // setAlteredQuestion(newAlteredQuestion);
    };

  const handleChangeText = (item: QuestionResponse) => (e: string) => {
    const newAlteredQuestion = [...(alteredQuestion || [])];
    const index = newAlteredQuestion.findIndex(f => f.id === item.id);
    if (index !== -1) {
      Object.assign(newAlteredQuestion[index], {
        result1: e,
      });
    }
    setValue(name, newAlteredQuestion);
  };
  return (
    <Fragment>
      <View style={styles.header}>
        <Text h4>{title}</Text>
      </View>

      <View style={styles.detail}>
        {fields.map((field: any) => (
          <ListItem hasTVPreferredFocus tvParallaxProperties key={field.id}>
            <ListItem.Content>
              <ListItem.Title>{field.qna_id[1]}</ListItem.Title>
            </ListItem.Content>
            <Controller
              name="kualitas_awal_line"
              control={control}
              render={() =>
                (field.type === 'input' && (
                  <ListItem.Input
                    autoCompleteType
                    placeholder=""
                    onChangeText={handleChangeText(field)}
                    disabled={disabled}
                    keyboardType="numeric"
                    value={field.result1}
                  />
                )) || (
                  <ListItem.CheckBox
                    iconType="ionicon"
                    checkedIcon="checkbox-outline"
                    uncheckedIcon="square-outline"
                    checkedColor={theme.colors?.primary}
                    checked={field.result}
                    onPress={handleSelect(field)}
                    disabled={disabled}
                  />
                )
              }
            />
          </ListItem>
        ))}
      </View>
    </Fragment>
  );
};

export default QuestionItem;
