import odoo from 'services/odoo.client';
import useSWR from 'swr';

const useCompany = (ids?: number) => {
  const { data } = useSWR(
    ids ? `/api/v1/company/by_name?ids=${ids}` : null,
    async () => {
      const res = await odoo.rpc_call(
        'call_kw/res.company/name_get',
        'res.company',
        {
          method: 'name_get',
          args: [[ids]],
          kwargs: {},
        },
      );

      return res?.result[0] || [];
    },
  );

  return {
    name: (data && data[1]) || '',
    id: (data && data[0]) || 0,
  };
};

export default useCompany;
