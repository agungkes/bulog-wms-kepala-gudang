import getDailyDefaultMonitoring from 'modules/PerawatanGudang/services/getDailyDefaultMonitoring';
import useSWR from 'swr';

const useDailyDefaultMonitoring = () => {
  const { data } = useSWR(
    '/api/v1/daily_monitoring/default',
    getDailyDefaultMonitoring,
  );

  return {
    data,
    isLoading: !data,
  };
};

export default useDailyDefaultMonitoring;
