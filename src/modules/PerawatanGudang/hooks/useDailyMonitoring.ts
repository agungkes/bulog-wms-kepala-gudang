import getDailyMonitoring from 'modules/PerawatanGudang/services/getDailyMonitoring';
import useSWRInfinite from 'swr/infinite';

const PAGE_SIZE = 10;
const getKey = (pageIndex: number, previousPageData: any) => {
  if (previousPageData && !previousPageData.length) {
    return null;
  }

  return `/api/get-daily-monitoring-list?page=${pageIndex}&limit=10&offset=${
    pageIndex * PAGE_SIZE
  }`;
};

const useDailyMonitoring = () => {
  const { data, size, setSize, error, mutate } = useSWRInfinite(
    getKey,
    getDailyMonitoring,
  );

  const isLoadingInitialData = !data && !error;
  const isLoadingMore =
    isLoadingInitialData ||
    (size > 0 && data && typeof data[size - 1] === 'undefined');
  const isEmpty = data?.[0]?.length === 0;
  const isReachingEnd =
    isEmpty || (data && data[data.length - 1]?.length < PAGE_SIZE);

  return {
    mutate,
    data: data || [],
    size,
    setSize,
    isReachingEnd,
    isLoadingMore,
  };
};

export default useDailyMonitoring;
