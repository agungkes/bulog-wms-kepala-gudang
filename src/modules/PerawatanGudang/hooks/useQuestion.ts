import odoo from 'services/odoo.client';
import useSWR from 'swr';

const useQuestion = ({ model, ids }: { model: string; ids: number[] }) => {
  const { data } = useSWR([model, ids], async () => {
    const res = await odoo.get<QuestionResponse[]>(model, {
      args: [ids, ['qna_id', 'type', 'result', 'result1']],
      kwargs: {},
    });

    return res || [];
  });

  return {
    data,
    loading: !data,
  };
};

export default useQuestion;
