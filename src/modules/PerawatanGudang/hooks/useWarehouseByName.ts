import odoo from 'services/odoo.client';
import useSWR from 'swr';

const useWarehouseByName = (ids?: number) => {
  const { data } = useSWR(
    ids ? `/api/v1/warehouse/by_name?ids=${ids}` : null,
    async () => {
      const res = await odoo.rpc_call(
        'call_kw/stock.warehouse/name_get',
        'stock.warehouse',
        {
          method: 'name_get',
          args: [[ids]],
          kwargs: {},
        },
      );

      return res?.result[0] || [];
    },
  );

  return {
    name: (data && data[1]) || '',
    id: (data && data[0]) || 0,
  };
};

export default useWarehouseByName;
