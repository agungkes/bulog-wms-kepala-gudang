import PerawatanGudangList from './PerawatanGudangList';
import PerawatanGudangDetail from './PerawatanGudangDetail';
import PerawatanGudangCreate from './PerawatanGudangCreate';

export { PerawatanGudangList, PerawatanGudangDetail, PerawatanGudangCreate };
