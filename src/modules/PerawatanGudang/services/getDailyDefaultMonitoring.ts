import odoo from 'services/odoo.client';

const getDailyDefaultMonitoring = async () => {
  const res = await odoo.rpc_call(
    'call_kw/vit.daily_monitoring/default_get',
    'vit.daily_monitoring',
    {
      method: 'default_get',
      args: [
        [
          'state',
          'name',
          'kepala_gudang_id',
          'warehouse_id',
          'location_id',
          'company_id',
          'date',
          'kualitas_awal_ids',
          'sanitasi_gudang_ids',
          'aerasi_line_ids',
          'pengosongan_gudang_ids',
        ],
      ],
      kwargs: {},
    },
  );

  return res?.result || {};
};

export default getDailyDefaultMonitoring;
