import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getDailyMonitoring = async (props: string) => {
  const pagination = getObjectFromSearch(props);
  const res = await odoo.search_read<GetDailyMonitoring[]>(
    'vit.daily_monitoring',
    {
      fields: [
        'id',
        'name',
        'unit_gudang',
        'month',
        'date',
        'state',
        'gudang_id',
        'kepala_gudang_id',
        'aerasi_line_ids',
        'company_id',
        'location_id',
        'pengosongan_gudang_ids',
        'sanitasi_gudang_ids',
        'warehouse_id',
        'kualitas_awal_ids',
      ],
      limit: Number(pagination.limit),
      offset: Number(pagination.offset),
      sort: 'date DESC',
    },
  );

  return res || [];
};

export default getDailyMonitoring;
