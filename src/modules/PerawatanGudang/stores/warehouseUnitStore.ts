import { observable } from 'mobx';

const warehouseUnitStore = observable({
  searchText: '',
  warehouseUnit: {
    id: 0,
    name: '',
  },
  setSearchText(value: string) {
    return (this.searchText = value);
  },
  setWarehouseUnit(value: { id: number; name: string }) {
    return (this.warehouseUnit = value);
  },
});

export default warehouseUnitStore;
