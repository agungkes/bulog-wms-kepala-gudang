import Layout from 'components/Layout';
import React from 'react';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './PickingList.styles';
import { useNavigation } from 'react-native-navigation-hooks';

import PickingItem from '../components/PickingItem';
import { FlatList } from 'react-native';

import usePickingList from 'modules/Picking/hooks/usePickingList';
import FlatListEmpty from 'components/FlatListEmpty';
import Loading from 'components/Loading';

type Props = {};
const PickingList: ScreenFC<Props> = () => {
  const navigation = useNavigation();
  const { data, loading, size, setSize } = usePickingList();

  const handleGoToDetail = (item: PickingData) => () => {
    navigation.push(screens.PICKING_DETAIL, item);
  };
  const renderItem = ({ item }: { item: PickingData }) => {
    return <PickingItem {...item} onPress={handleGoToDetail(item)} />;
  };
  const keyExtractor = (item: PickingData) => `${item.id}`;

  const onEndReached = () => {
    setSize(size + 1);
  };
  return (
    <Layout>
      <FlatList
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        data={data.flat(1)}
        contentContainerStyle={styles.PickingListWrapper}
        alwaysBounceVertical
        scrollEventThrottle={16}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.5}
        ListEmptyComponent={FlatListEmpty}
      />

      <Loading isVisible={loading} />
    </Layout>
  );
};

PickingList.screenName = screens.PICKING_LIST;
PickingList.options = () => ({
  topBar: {
    title: {
      text: 'Picking',
    },
  },
});
export default PickingList;
