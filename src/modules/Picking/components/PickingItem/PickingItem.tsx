import React from 'react';
import { View } from 'react-native';
import styles from './PickingItem.styles';

import DropShadow from 'react-native-drop-shadow';
import { Divider, ListItem, Text } from 'react-native-elements';
import Item from 'components/Item';
import state from 'helpers/state';

type Props = PickingData & {
  onPress?: () => void;
};
const PickingItem: React.FC<Props> = ({ onPress, ...props }) => (
  <DropShadow style={styles.container}>
    <ListItem hasTVPreferredFocus tvParallaxProperties onPress={onPress}>
      <ListItem.Content>
        <View style={styles.header}>
          <Text style={styles.headerTextSurat}>{props.name}</Text>
          <Text style={styles.headerTextDate}>{state[props.state]}</Text>
        </View>
        <Divider style={styles.divider} />

        <Item
          title="Destination Location"
          value={props.pick_dest_location_id}
        />
        <Item title="Partner" value={props.partner_id} />
        <Item title="Scheduled Date" value={props.date} />
        <Item title="Source Document" value={props.pick_origin} />
      </ListItem.Content>
    </ListItem>
  </DropShadow>
);

export default PickingItem;
