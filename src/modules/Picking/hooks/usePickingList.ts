import { getPickingList } from 'modules/Picking/services';
import useSWRInfinite from 'swr/infinite';

const PAGE_SIZE = 10;
const getKey = (pageIndex: number, previousPageData: any) => {
  if (previousPageData && !previousPageData.length) {
    return null;
  } // reached the end

  return `/api/get-picking-list?page=${pageIndex}&limit=10&offset=${
    pageIndex * 10
  }`;
};

const usePickingList = () => {
  const { data, size, setSize, error, ...rest } = useSWRInfinite(
    getKey,
    getPickingList,
  );

  const isLoadingInitialData = !data && !error;
  const isLoadingMore =
    isLoadingInitialData ||
    (size > 0 && data && typeof data[size - 1] === 'undefined');
  const isEmpty = data?.[0]?.length === 0;
  const isReachingEnd =
    isEmpty || (data && data[data.length - 1]?.length < PAGE_SIZE);

  return {
    data: data || [],
    loading: !!isLoadingMore && !isReachingEnd,
    size,
    setSize,
    ...rest,
  };
};
export default usePickingList;
