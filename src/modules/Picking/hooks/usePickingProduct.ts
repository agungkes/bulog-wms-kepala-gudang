// import { getPickingProducts } from 'modules/Picking/services';
import { getPutawayProduct } from 'modules/Putaway/services';
import useSWR from 'swr';

const usePickingProduct = (ids: number | number[]) => {
  const { data, ...rest } = useSWR<PickingProduct[]>(
    `/api/picking/products/?ids=${ids}`,
    getPutawayProduct,
  );

  return {
    ...rest,
    data: data || [],
    loading: !data,
  };
};

export default usePickingProduct;
