import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    padding: widthPercentageToDP(4),
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    textAlign: 'center',
  },

  detail: {
    marginVertical: heightPercentageToDP(1),
  },

  approveBtn: {
    marginBottom: heightPercentageToDP(1),
  },

  header2: {
    backgroundColor: '#fff',
    shadowColor: '#000000',
    paddingVertical: heightPercentageToDP(2),
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelSearch: {
    width: '100%',
    backgroundColor: '#fff',
    paddingHorizontal: 10,
  },
});

export default styles;
