import ItemDetail from 'components/ItemDetail';
import Layout from 'components/Layout';
import Loading from 'components/Loading';
import state from 'helpers/state';
import useLoadingScreen from 'hooks/useLoadingScreen';
// import useToggle from 'hooks/useToggle';
import get from 'lodash.get';
import { observer } from 'mobx-react-lite';
import useDestinationLocation from 'modules/Putaway/hooks/useDestinationLocation';
import usePutawayList from 'modules/Putaway/hooks/usePutawayList';
import usePutawayProductList from 'modules/Putaway/hooks/usePutawayProductList';
import destinationStore from 'modules/Putaway/stores/destinationStore';
import React, { Fragment, useEffect, useState } from 'react';
import { Alert, Keyboard, KeyboardEvent, Pressable, View } from 'react-native';
import { Button, ListItem, Text } from 'react-native-elements';
import {
  ScrollView,
  TextInput,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import { Navigation } from 'react-native-navigation';
import { RNNBottomSheet } from 'react-native-navigation-bottom-sheet';
import { useNavigationComponentDidDisappear } from 'react-native-navigation-hooks/dist';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import screens from 'screens';
import odoo from 'services/odoo.client';
import styles from './PutawayDetail.styles';

const SearchInput = () => {
  const [s] = useState(destinationStore);

  const handleChangeSearchText = (value: string) => {
    s.setSearchText(value);
  };
  return (
    <TextInput
      style={{
        borderColor: '#9f9f9f',
        borderWidth: 1,
      }}
      autoFocus
      placeholder="Destination Location"
      onChangeText={handleChangeSearchText}
      key="searchInput"
    />
  );
};

const RenderHeader = () => {
  return (
    <View style={styles.header2}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
        <View style={styles.panelSearch}>
          <SearchInput />
        </View>
      </View>
    </View>
  );
};

const SearchList = observer(() => {
  const [s] = useState(destinationStore);
  const { data = [] } = useDestinationLocation(s.searchText);

  const handleSelectDestination = (value: [number, string]) => () => {
    s.setDestinationLocation({
      id: value[0],
      name: value[1],
    });
    Keyboard.dismiss();
    RNNBottomSheet.closeBottomSheet();
  };
  return (
    <Fragment>
      {data?.map(item => (
        <TouchableOpacity
          activeOpacity={0.6}
          style={{
            padding: heightPercentageToDP(2),
          }}
          onPress={handleSelectDestination(item)}
          key={`${item[0]}`}>
          <Text>{item[1]}</Text>
        </TouchableOpacity>
      ))}
    </Fragment>
  );
});
const RenderContent = () => {
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const onKeyboardDidShow = (e: KeyboardEvent) => {
    setKeyboardHeight(e.endCoordinates.height);
  };

  function onKeyboardDidHide() {
    setKeyboardHeight(0);
  }

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', onKeyboardDidShow);
    Keyboard.addListener('keyboardDidHide', onKeyboardDidHide);
    return () => {
      Keyboard.removeAllListeners('keyboardDidShow');
      Keyboard.removeAllListeners('keyboardDidHide');
    };
  }, []);
  return (
    <View
      style={{
        paddingBottom: keyboardHeight + heightPercentageToDP(3),
      }}>
      <SearchList />
    </View>
  );
};

type PutawayDetailProps = PutawayData & {};
const PutawayDetail: ScreenFC<PutawayDetailProps> = props => {
  const [s] = useState(destinationStore);

  const { mutate } = usePutawayList();
  const { data: products = [], loading } = usePutawayProductList(
    props.move_ids,
  );

  const handleClearDestinationStore = () => {
    s.setDestinationLocation({
      id: 0,
      name: '',
    });
  };
  useNavigationComponentDidDisappear(handleClearDestinationStore);

  const [showLoading, dismissLoading] = useLoadingScreen();

  const handleShowDestinationLocation = () => {
    if (props.state === 'draft') {
      RNNBottomSheet.openBottomSheet({
        renderContent: () => <RenderContent />,
        renderHeader: RenderHeader,
        snapPoints: [0, '20%', '40%', '80%'],
        borderRadius: 16,
      });
    }
  };

  const handleValidate = async () => {
    try {
      await showLoading();
      const res = await odoo.rpc_call('call_button', 'vit.putaway', {
        method: 'action_approve',
        args: [[props.id]],
      });

      if (res.error) {
        throw new Error(res.error.data.message);
      }
      Alert.alert('Success', 'Berhasil melakukan approve putaway');
    } catch (error: any) {
      console.log(error);
      Alert.alert('Error', error.message);
    } finally {
      dismissLoading();
    }
  };

  const handleSave = async () => {
    try {
      await showLoading();
      if (s.destinationLocation.id) {
        const updatedMoveIds = [...products].map(product => {
          return [
            1,
            product.id,
            {
              location_dest_id: s.destinationLocation.id,
            },
          ];
        });

        const res = await odoo.rpc_call(
          'call_kw/vit.putaway/write/',
          'vit.putaway',
          {
            method: 'write',
            args: [
              [props.id],
              {
                put_dest_location_id: s.destinationLocation.id,
                move_ids: updatedMoveIds,
              },
            ],
            kwargs: {},
          },
        );

        if (res.error) {
          throw new Error(res.error.data.message);
        }

        Navigation.updateProps(props.componentId, {
          put_dest_location_id: [
            s.destinationLocation.id,
            s.destinationLocation.name,
          ],
        });

        await mutate(prev => {
          return prev?.map(e => {
            return e.map(f => {
              const newData = { ...f };

              console.log(f.id, props.id);
              if (f.id === props.id) {
                newData.put_dest_location_id = [
                  s.destinationLocation.id,
                  s.destinationLocation.name,
                ];

                console.log(newData);
              }

              return newData;
            });
          });
        }, false);
        Alert.alert('Success', 'Berhasil menyimpan data');
      }
    } catch (error: any) {
      console.log(error);
      Alert.alert('Error', error.message);
    } finally {
      dismissLoading();
    }
  };
  return (
    <Layout>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            {props.name}
          </Text>
        </View>

        <View style={styles.detail}>
          <ItemDetail title="Status" value={state[props.state]} />
          <ItemDetail title="No PO" value={props.put_origin} />
          <ItemDetail title="Partner" value={props.partner_id} />
          <ItemDetail
            title="Source Location"
            value={props.put_source_location_id}
          />
          <Pressable onPress={handleShowDestinationLocation}>
            <ItemDetail
              title="Destination Location"
              value={s.destinationLocation.name || props.put_dest_location_id}
              editable
              inputEditable={false}
            />
          </Pressable>

          <ItemDetail title="Nama Mandor" value={props.leader_name} />
          <ItemDetail title="Jumlah Buruh" value={props.work_number} />
          <ItemDetail title="Jenis Alat" value={props.tools_name} />
          <ItemDetail title="Jumlah Alat" value={props.tools_number} />
        </View>

        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            Products
          </Text>
        </View>
        <View style={styles.detail}>
          {products.map(product => (
            <ListItem key={product.id} hasTVPreferredFocus tvParallaxProperties>
              <ListItem.Content>
                <ListItem.Title>
                  {get(product, 'product_id.1', '') || ''}
                </ListItem.Title>
                <ListItem.Subtitle>
                  Initial Demand: {get(product, 'product_uom_qty')}
                </ListItem.Subtitle>
                <ListItem.Subtitle>
                  Done: {get(product, 'quantity_done')}
                </ListItem.Subtitle>
                <ListItem.Subtitle>
                  Unit of Measure: {get(product, 'product_uom_id')}
                </ListItem.Subtitle>
              </ListItem.Content>
            </ListItem>
          ))}
        </View>

        <View>
          {props.state === 'draft' && (
            <Fragment>
              <Button
                containerStyle={{ marginBottom: heightPercentageToDP(1) }}
                title={'Save'}
                onPress={handleSave}
              />
              <Button
                title={'Approve'}
                containerStyle={styles.approveBtn}
                onPress={handleValidate}
              />
            </Fragment>
          )}
        </View>
      </ScrollView>

      <Loading isVisible={loading} />
    </Layout>
  );
};

PutawayDetail.screenName = screens.PUTAWAY_DETAIL;
PutawayDetail.options = props => {
  return {
    topBar: {
      title: {
        text: props.name || '',
      },
    },
  };
};
export default observer(PutawayDetail);
