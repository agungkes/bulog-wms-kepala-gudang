import { getDestinationLocation } from 'modules/Putaway/services';
import useSWR from 'swr';

const useDestinationLocation = (name?: string) => {
  const { data } = useSWR(
    `/api/get-destination-location/?name=${name}`,
    getDestinationLocation,
  );

  return {
    data: data || [],
    loading: !data,
  };
};

export default useDestinationLocation;
