import { getPutawayProduct } from 'modules/Putaway/services';
import useSWR from 'swr';

const usePutawayProductList = (ids: number | number[]) => {
  //   const { data, ...rest } = useSWR(ids ? `/api/putaway/products/${ids}` : null);
  const { data, ...rest } = useSWR(
    `/api/putaway/products/?ids=${ids}`,
    getPutawayProduct,
  );

  return {
    ...rest,
    data: data || [],
    loading: !data,
  };
};

export default usePutawayProductList;
