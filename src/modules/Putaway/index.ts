import PutawayList from './PutawayList';
import PutawayDetail from './PutawayDetail';

export { PutawayList, PutawayDetail };
