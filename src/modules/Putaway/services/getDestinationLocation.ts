import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getDestinationLocation = async (props: string) => {
  const obj = getObjectFromSearch(props);

  const res = await odoo.rpc_call<{ result: [number, string][] }>(
    'call_kw/stock.location/name_search',
    'stock.location',
    {
      args: [],
      kwargs: {
        args: [],
        limit: 80,
        name: obj.name,
        operator: 'ilike',
      },
      method: 'name_search',
    },
  );

  const returnedData = res?.result || [];

  return returnedData;
};

export default getDestinationLocation;
