import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getPutawayList = async (props: string) => {
  const pagination = getObjectFromSearch(props);

  const data = await odoo.search_read<PutawayData[]>('vit.putaway', {
    context: { lang: 'en_US', tz: 'Asia/Jakarta' },
    fields: [
      'name',
      'put_dest_location_id',
      'partner_id',
      'state',
      'put_picking_id',
      'put_source_location_id',
      'date',
      'put_origin',
      'picking_type_code',
      'move_ids',
      'leader_name',
      'work_number',
      'tools_number',
      'tools_name',
      'company_id',
      'picking_type_id',
      'note',
      'message_follower_ids',
      'message_ids',
      'display_name',
    ],
    sort: 'id DESC',
    limit: Number(pagination.limit),
    offset: Number(pagination.offset),
  });

  return data;
};

export default getPutawayList;
