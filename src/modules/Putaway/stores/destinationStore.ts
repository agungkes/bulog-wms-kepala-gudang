import { observable } from 'mobx';

const destinationStore = observable({
  searchText: '',
  destinationLocation: {
    id: 0,
    name: '',
  },
  setSearchText(value: string) {
    return (this.searchText = value);
  },
  setDestinationLocation(value: { id: number; name: string }) {
    return (this.destinationLocation = value);
  },
});

export default destinationStore;
