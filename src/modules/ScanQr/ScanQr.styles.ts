import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

/* your css code */
const styles = StyleSheet.create({
  container: {
    // flex: 1,
    flexDirection: 'column',
    height: '100%',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  flashlightContainer: {
    position: 'absolute',
    bottom: heightPercentageToDP(5),
    borderColor: 'rgba(255,255,255,0.5)',
    borderWidth: widthPercentageToDP(1),
    borderRadius: heightPercentageToDP(10),
    padding: heightPercentageToDP(1),
  },
});

export default styles;
