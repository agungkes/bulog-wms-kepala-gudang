import React from 'react';
import { Alert, Pressable, View } from 'react-native';
import { Icon, useTheme } from 'react-native-elements';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './ScanQr.styles';

import { BarCodeReadEvent, RNCamera } from 'react-native-camera';
import { BarcodeMask } from '@nartc/react-native-barcode-mask';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import useToggle from '@hooks/useToggle';
import { useNavigation } from 'react-native-navigation-hooks';
import useLoadingScreen from 'hooks/useLoadingScreen';
import odoo from 'services/odoo.client';

const ScanQr: ScreenFC = () => {
  const { theme } = useTheme();
  const [showFlashlight, setShowFlashlight] = useToggle();
  const [showLoading, dismissLoading] = useLoadingScreen();

  const [active, toggleActive] = useToggle(true);

  const navigation = useNavigation();

  const handleBarcodeRead = async (event: BarCodeReadEvent) => {
    try {
      if (event.data) {
        toggleActive();
        await showLoading();

        const res = await odoo.search_read('stock.picking', {
          fields: [
            'name',
            'location_id',
            'location_dest_id',
            'partner_id',
            'date',
            'scheduled_date',
            'origin',
            'ref_fax',
            'group_id',
            'backorder_id',
            'state',
            'state_inv',
            'invoice_paid',
            'priority',
            'picking_type_id',
            'batch_id',
            'move_lines',
            'jenis_operasi',
          ],
          domain: [['name', 'ilike', event.data]],
        });

        if (res.error) {
          throw new Error(res.error.data.message);
        }

        if (res.length === 0) {
          Alert.alert(
            'Data Tidak Ditemukan',
            'Data tidak ditemukan, pastikan data yang anda scan sudah benar',
            [
              {
                text: 'OK',
                onPress: () => {
                  dismissLoading();
                  toggleActive();
                },
              },
            ],
            { cancelable: false },
          );
          return;
        }

        dismissLoading();
        toggleActive();
        navigation.push(screens.INCOMING_DETAIL, (res && res[0]) || {});
      }
    } catch (error: any) {
      console.log(error);
      Alert.alert('Error', error.message);
    }
    // console.log(event);
    // navigation.push(screens.INCOMING_DETAIL, {

    // })
  };

  if (!active) {
    return null;
  }
  return (
    <View style={styles.container}>
      <RNCamera
        autoFocus={RNCamera.Constants.AutoFocus.on}
        style={styles.preview}
        type={RNCamera.Constants.Type.back}
        flashMode={
          showFlashlight
            ? RNCamera.Constants.FlashMode.torch
            : RNCamera.Constants.FlashMode.off
        }
        captureAudio={false}
        onBarCodeRead={handleBarcodeRead}>
        <BarcodeMask
          width={300}
          height={200}
          animatedLineThickness={1}
          backgroundColor={theme.colors?.primary}
          maskOpacity={0.3}
          edgeColor={theme.colors?.primary}
          animatedLineColor={theme.colors?.accent}
        />
        <Pressable
          onPress={setShowFlashlight}
          style={styles.flashlightContainer}>
          {(showFlashlight && (
            <Icon
              name="flash-off"
              type="ionicon"
              tvParallaxProperties
              hasTVPreferredFocus
              size={widthPercentageToDP(10)}
              color={'rgba(255,255,255,1)'}
            />
          )) || (
            <Icon
              name="flash"
              type="ionicon"
              tvParallaxProperties
              hasTVPreferredFocus
              size={widthPercentageToDP(10)}
              color={'rgba(255,255,255,0.5)'}
            />
          )}
        </Pressable>
      </RNCamera>
    </View>
  );
};

ScanQr.screenName = screens.SCAN_QR;
ScanQr.options = () => {
  return {
    topBar: {
      visible: false,
      drawBehind: true,
      background: {
        color: '#02418b',
      },
    },
  };
};
export default ScanQr;
