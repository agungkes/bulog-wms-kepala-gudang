import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    padding: widthPercentageToDP(4),
  },

  detail: {
    marginVertical: heightPercentageToDP(1),
  },
});

export default styles;
