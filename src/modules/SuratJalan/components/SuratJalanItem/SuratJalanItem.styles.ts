import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    shadowColor: '#999999',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    marginBottom: heightPercentageToDP(2),
  },
  divider: {
    width: '100%',
    marginVertical: 10,
  },

  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  headerTextDate: {
    fontSize: widthPercentageToDP(3.5),
  },
  headerTextSurat: {
    fontWeight: 'bold',
  },
});

export default styles;
