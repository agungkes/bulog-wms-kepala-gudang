import odoo from 'services/odoo.client';

const getStockPickingType = async () => {
  const stockPickingType = await odoo.search_read('stock.picking.type', {
    domain: [],
    fields: ['warehouse_id'],
    limit: 80,
  });

  return stockPickingType;
};

export default getStockPickingType;
