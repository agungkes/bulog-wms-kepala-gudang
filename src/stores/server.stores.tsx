// counter.store.js
import React from 'react';
import { makeAutoObservable } from 'mobx';
import AsyncStorage from '@react-native-community/async-storage';
import {
  clearPersistedStore,
  getPersistedStore,
  hydrateStore,
  isHydrated,
  makePersistable,
} from 'mobx-persist-store';

class ServerStore {
  serverDetail = {
    domain: '',
    database: '',
    protocol: '',
    sid: '',
    username: '',
    password: '',
  };

  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
    makePersistable(this, {
      name: 'ServerStore',
      storage: AsyncStorage,
      stringify: true,
      removeOnExpiration: true,
      properties: ['serverDetail'],
    });
  }

  set setServer(server: any) {
    this.serverDetail = {
      ...this.serverDetail,
      ...server,
    };
  }

  get getServer() {
    return this.serverDetail;
  }

  get odooConnection() {
    return {
      host: this.serverDetail.domain,
      protocol: this.serverDetail.protocol,
      database: this.serverDetail.database,
      username: this.serverDetail.username,
      password: this.serverDetail.password,
    };
  }

  get isHydrated() {
    return isHydrated(this);
  }

  async hydrateStore() {
    await hydrateStore(this);
  }

  async getStoredData() {
    return getPersistedStore(this);
  }

  async clearStoredDate() {
    this.serverDetail = {
      domain: '',
      database: '',
      protocol: '',
      sid: '',
      username: '',
      password: '',
    };
    await clearPersistedStore(this);
  }
}

// Instantiate the counter store.
export const storeServer = new ServerStore();

// // Create a React Context with the counter store instance.
export const StoreContext = React.createContext(storeServer);
export const useServerStore = () => React.useContext(StoreContext);
export const ServerProvider: React.FC = ({ children }) => {
  return (
    <StoreContext.Provider value={storeServer}>
      {children}
    </StoreContext.Provider>
  );
};
